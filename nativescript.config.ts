import { NativeScriptConfig } from '@nativescript/core';

export default {
  id: 'org.nativescript.mydrawer',
  appResourcesPath: 'App_Resources',
  android: {
    v8Flags: '--expose_gc',
    markingMode: 'none'
  }
} as NativeScriptConfig;